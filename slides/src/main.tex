% Preamble
\documentclass[169]{beamer}
\usetheme{metropolis}

% Packages
\usepackage{amsmath}
\usepackage{shellesc}
\usepackage[outputdir=../auxil]{minted}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{graphicx}

\title{Introduction to text parsing}
\subtitle{with Boost.Spirit.X3}
\date{\today}
\author{Karthik Nishanth}
\institute{Zivid}

% Global configuration
\metroset{block=fill}
\definecolor{srcbg}{rgb}{0.95,0.95,0.95}
\setminted{fontsize=\scriptsize,autogobble,bgcolor={srcbg}}

% Document
\begin{document}

    \maketitle

    \section{Why parse text?}

    \subsection{Where is text parsed}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item Programming languages
            \item Configuration files
            \item Domain-specific data storage
            \item Competitive programming
        \end{itemize}
    \end{frame}

    \begin{frame}[fragile]{\subsecname}
        Advent of Code, Day 7 \\
        \begin{minted}{text}
            dotted blue bags contain 5 wavy green bags, 3 pale beige bags.
            dull lime bags contain 1 dotted olive bag, 3 dim brown bags.
            drab olive bags contain 2 shiny coral bags.
            dim orange bags contain 1 faded turquoise bag.
        \end{minted}
    \end{frame}

    \begin{frame}[fragile]{\subsecname}
        Advent of Code, Day 8 \\
        \begin{minted}{text}
            acc +33
            acc -7
            acc +39
            jmp +214
            jmp +250
            jmp +51
            acc +29
            acc +6
            acc +20
        \end{minted}
    \end{frame}

    \begin{frame}[fragile]{\subsecname}
        Advent of Code, Day 4 \\
        \begin{minted}{text}
            hcl:#fffffd ecl:gry eyr:2022
            hgt:172cm pid:781914826 byr:1930 iyr:2018

            pid:188cm byr:2005
            hgt:170cm cid:163 ecl:#a08502 hcl:2964fb eyr:1994
            iyr:2005
        \end{minted}
    \end{frame}

    \begin{frame}[fragile]{\subsecname}
        Advent of Code, Day 18 \\
        \begin{minted}{text}
            7 * 8 + 2 + 8 * (8 * 4) * (4 + 8)
            (9 + 3 + 2 * 5 * 8) + 9 + 5 * 2 * 5 * (6 * 6 * 4 + 6 * 9 * 3)
            6 * 6 * 4 * (6 + (3 * 9 * 2) + 9 + (4 + 7 + 7))
            (8 + 9 * 7 * 9 + 6) + (7 + 5) + 6
            5 * (6 * (3 + 2 + 9)) + 8 + 3 + 5 * (4 + 3 * 8 * 8 * 6 * 2)
            4 + (3 + 4 * (4 + 9 + 3) * (4 * 8 * 4 * 9))
            4 * 4 * 6 * 2 + ((8 * 4 * 3 + 6) * 5) + 6
        \end{minted}
    \end{frame}

    \subsection{Learning outcome}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item Parsing complicated text with Boost Spirit
            \item Boost UT
            \item Prep for Advent of Code next year!
        \end{itemize}
    \end{frame}

    \section{Grammars}

    \subsection{Boost.Spirit}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item Recursive descent parser generator
            \item Represented as extended Backus-Naur form (EBNF) in C++
            \item Generates top-down, unlimited lookahead parsers
        \end{itemize}
    \end{frame}

    \subsection{Extended Backus-Naur Form}
    \begin{frame}[fragile]{\subsecname}
        \begin{itemize}
            \item Formal grammar describing a language
            \item Can be converted into recursive descent parsers
        \end{itemize}

        \begin{minted}{text}
            group ::= '(' expression ')'
            factor ::= integer | group
            term ::= factor (('*' factor) | ('/' factor))*
            expression ::= term (('+' term) | ('-' term))*
        \end{minted}
    \end{frame}

    \subsection{Spirit parser}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item Greedy: \texttt{*char >> char}
            \item Non exhaustive backtracking, unlike RegEx
            \item Backtracks only when a sub-parser fails
        \end{itemize}
    \end{frame}

    \subsection{Spirit components}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item \makebox[1.5cm]{Qi\hfill} - Parser generator - Text to data structure
            \item \makebox[1.5cm]{Karma\hfill} - Printer - Data structure to Text
            \item \makebox[1.5cm]{Lex\hfill} - Lexer - Text to tokens
            \item \makebox[1.5cm]{X3\hfill} - Modern Qi
        \end{itemize}
    \end{frame}

    \subsection{Is X3 heavy?}
    \begin{frame}[fragile]{\subsecname}
        \begin{columns}
            \begin{column}{0.70\textwidth}
                \begin{minted}{cpp}
                    constexpr std::string_view text = "abc";
                    auto p = char_ >> char_ >> char_;
                    return parse(text.begin(), text.end(), p);
                \end{minted}
            \end{column}
            \begin{column}{0.30\textwidth}
                \begin{minted}{asm}
                    main:
                    mov eax, 1
                    ret
                \end{minted}
            \end{column}
        \end{columns}
        \begin{columns}
            \begin{column}{0.70\textwidth}
                \begin{minted}{cpp}
                    constexpr std::string_view text = "ab";
                    auto p = char_ >> char_ >> char_;
                    return parse(text.begin(), text.end(), p);
                \end{minted}
            \end{column}
            \begin{column}{0.30\textwidth}
                \begin{minted}{asm}
                    main:
                    xor eax, eax
                    ret
                \end{minted}
            \end{column}
        \end{columns}
    \end{frame}

    \subsection{Component libraries}
    \begin{frame}[fragile]{\subsecname}
        Spirit parser attributes support
        \begin{itemize}
            \item STL
            \item Boost Variant, Tuple
            \item Fusion-compatible objects
        \end{itemize}
        \begin{minted}{cpp}
            struct Fruit {
            std::string name;
            std::vector<int> farmIds;
            };

            BOOST_FUSION_ADAPT_STRUCT(Fruit, name, farmIds);

            Fruit fruit;
            parse(it, end, word_ >> *int_, fruit);
        \end{minted}
    \end{frame}


    \section{Using Spirit.X3}
    \input{using-spirit-x3}


    \section{Hands-on workshop}
    \input{hands-on-workshop}

    \subsection{Inspired by \& Further reading}
    \begin{frame}{\subsecname}
        \begin{center}
            \includegraphics[width=0.75\textwidth]{assets/spirit-cppcon.jpg}
        \end{center}
        CppCon 2015: Michael Caisse: Using Spirit X3 to Write Parsers \\
        \url{https://www.youtube.com/watch?v=xSBWklPLRvw}
    \end{frame}

    \subsection{My solutions}
    \begin{frame}{\subsecname}
        \begin{center}
            \url{https://gitlab.com/nishanthkarthik/spiritx3-presentation/-/tree/solution}
        \end{center}
    \end{frame}
\end{document}
