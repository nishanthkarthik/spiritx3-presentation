#include "driver.h"
#include <map>

// Maps

using Stylesheet = std::map<std::string, std::map<std::string, std::string>>;

Stylesheet styles(const std::string &input)
{
    using namespace boost::spirit::x3;
    return {};
}

constexpr auto test0 = R"(
header {
    border: red;
    color: #eeffaa;
}

body {
    font-size: 15pt;
    border-width: 0;
}
)";

const auto result0 = Stylesheet {
    {
        "header",
        std::map<std::string, std::string> {
            { "border", "red" },
            { "color", "#eeffaa" },
        },
    },
    {
        "body",
        std::map<std::string, std::string> {
            { "border-width", "0" },
            { "font-size", "15pt" },
        },
    },
};

int main()
{
    using namespace boost::ut;
    "styles"_test = [] {
        expect(styles(test0) == result0);
    };
}