#include "driver.h"

// Dissimilar structures

int cpu(const std::string& input)
{
    using namespace boost::spirit::x3;
    return {};
}

constexpr auto test0 = R"(
add 1
add 3
)";

constexpr auto test1 = R"(
add 6
add -3
nop
add 21
nop
)";

int main() {
    using namespace boost::ut;
    "cpu"_test = [] {
        expect(cpu(test0) == 4_i);
        expect(cpu(test1) == 24_i);
    };
}