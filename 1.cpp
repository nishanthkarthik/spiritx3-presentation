#include "driver.h"

// Primitives

size_t square(const std::string& input)
{
    using namespace boost::spirit::x3;
    return {};
}

int main()
{
    using namespace boost::ut;
    "square"_test = [] {
        expect(square("3") == 9_ul);
        expect(square("25") == 625_ul);
        expect(square("-1") == 1_ul);
    };
}