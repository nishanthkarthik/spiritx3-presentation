#include "driver.h"

// Nested Lists

int matrix_dim(const std::string &input)
{
    using namespace boost::spirit::x3;
    return {};
}

constexpr auto oneDim = R"(
1
)";

constexpr auto twoDim = R"(
1 0
3 4
)";

constexpr auto threeDim = R"(
1 0 0
0 1 0
0 0 1
)";

int main()
{
    using namespace boost::ut;
    "matrix_dim"_test = [] {
        expect(matrix_dim(oneDim) == 1_i);
        expect(matrix_dim(twoDim) == 2_i);
        expect(matrix_dim(threeDim) == 3_i);
    };
}