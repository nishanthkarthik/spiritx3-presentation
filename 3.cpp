#include "driver.h"

// Nesting sequences

std::complex<int> complex_sum(const std::string& input)
{
    using namespace boost::spirit::x3;
    return {};
}

int main()
{
    using namespace boost::ut;
    "complex_sum"_test = [] {
        expect(complex_sum("(0, 1) + (-3, 4)") == std::complex(-3, 5));
        expect(complex_sum("(-1, 3) + (1, -3)") == std::complex(0, 0));
        expect(complex_sum("(-3, 4) + (-3, -4)") == std::complex(-6, 0));
    };
}