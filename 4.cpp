#include "driver.h"

// Lists

int sum(const std::string& input)
{
    using namespace boost::spirit::x3;
    return {};
}

int main() {
    using namespace boost::ut;
    "sum"_test = [] {
        expect(sum("1 2 3 4 5") == 15_i);
        expect(sum("1 1 1 1 1 1 1") == 7_i);
        expect(sum("1 0 1 0 1 0 1") == 4_i);
    };
}