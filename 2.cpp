#include "driver.h"

// Sequences

double abs_complex(const std::string& input)
{
    using namespace boost::spirit::x3;
    return {};
}

int main()
{
    using namespace boost::ut;
    "abs_complex"_test = [] {
        expect(abs_complex("(0, 1)") == 1.0_d);
        expect(abs_complex("(-3, 4)") == 5.0_d);
        expect(abs_complex("(-1, -1)") == 1.414_d);
    };
}